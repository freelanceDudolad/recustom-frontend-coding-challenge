import React from "react";
import ToastIcon from "./ToastIcon";
import "./Toast.css";
import avatarImg from "../assets/avatar.png";
import warningImg from "../assets/warning.svg";
import warningActionImg from "../assets/warning_action.svg";
import successImg from "../assets/success.svg";
import successCtaImg from "../assets/success_cta.svg";

export interface ToastProps {
    type: ToastType;
    message: string;
    action?: boolean;
    mobile?: boolean;
    description?: string;
}

export enum ToastType {
    SUCCESS = "success",
    WARNING = "warning",
    AVATAR = "avatar",
}

const colors = {
    success: "#00AC80",
    warning: "#FF6464",
    avatar: "#9CA3AF",
};

const images: { [K in ToastType]: string } = {
    [ToastType.SUCCESS]: successImg.src,
    [ToastType.WARNING]: warningImg.src,
    [ToastType.AVATAR]: avatarImg.src,
};

const actionImages: { [K in ToastType]?: string } = {
    [ToastType.SUCCESS]: successCtaImg.src,
    [ToastType.WARNING]: warningActionImg.src,
};

const getWidth = (mobile: boolean) => (mobile ? "341px" : "606px");

const getImage = (type: ToastType, description?: string): string => {
    if (description && type in actionImages) {
        return actionImages[type]!;
    }
    return images[type];
};

const Toast = ({
                   type,
                   message,
                   description,
                   action = false,
                   mobile = false,
               }: ToastProps) => {
    const startImage = getImage(type, description);
    const baseClass = "notification";

    return (
        <div
            className={`${baseClass} ${baseClass}--${type}`}
            style={{ width: getWidth(mobile) }}
        >
            <div
                className={`${baseClass}__message-wrapper`}
                style={{ alignItems: mobile ? "flex-start" : "center" }}
            >
                <img
                    src={startImage}
                    alt={type}
                    className={`${baseClass}__start-image`}
                    style={{ width: type === ToastType.AVATAR ? "32px" : "auto" }}
                />
                <div
                    className={`${baseClass}__message ${baseClass}__message--${type}`}
                    style={{
                        width: mobile ? "271px" : "auto",
                        fontWeight: action ? 600 : "normal",
                    }}
                    dangerouslySetInnerHTML={{ __html: message }}
                />
                <ToastIcon color={colors[type]} />
            </div>
            {description && (
                <div
                    className={`${baseClass}__wrapper-desc ${baseClass}__wrapper-desc--${type}`}
                >
                    <div className={`${baseClass}__description`}>{description}</div>
                    {action && (
                        <button
                            className={`${baseClass}__action ${baseClass}__action--${type}`}
                            data-testid="action-type"
                        >
                            {type === ToastType.AVATAR ? "Button Text" : "Take action"}
                        </button>
                    )}
                </div>
            )}
        </div>
    );
};

export default Toast;
