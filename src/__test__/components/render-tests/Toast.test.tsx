import { describe, it, expect } from "vitest";
import { render, screen } from "@testing-library/react";
import { ToastType } from "@/stories/Toast/Toast";
import "@testing-library/jest-dom";
import Toast from '../../../stories/Toast/Toast';

describe("Notification", () => {
    it("renders a success message", () => {
        render(<Toast type={ToastType.SUCCESS} message="Success message" />);
        expect(screen.getByText('Success message')).toBeInTheDocument();
        expect(screen.queryByTestId("action-type")).toBeNull();
    });

    it("renders a success message with an action and description", () => {
        const description = "This is an important success message";
        render(
            <Toast
                type={ToastType.SUCCESS}
                message="Success with action"
                action={true}
                description={description}
            />,
        );
        expect(screen.getByText("Success with action")).toBeInTheDocument();
        expect(screen.getByText(description)).toBeInTheDocument();
        expect(screen.getByTestId("action-type")).toBeInTheDocument();
    });

    it("renders a warning message", () => {
        render(<Toast type={ToastType.WARNING} message="Warning message" />);
        expect(screen.getByText("Warning message")).toBeInTheDocument();
        expect(screen.queryByTestId("action-type")).toBeNull();
    });

    it("renders a warning message with an action and description", () => {
        const description = "This is an important warning message";
        render(
            <Toast
                type={ToastType.WARNING}
                message="Warning with action"
                description={description}
                action={true}
            />,
        );
        expect(screen.getByText("Warning with action")).toBeInTheDocument();
        expect(screen.getByText(description)).toBeInTheDocument();
        expect(screen.getByTestId("action-type")).toBeInTheDocument();
    });

    it("renders an avatar message", () => {
        render(<Toast type={ToastType.AVATAR} message="Avatar message" />);
        expect(screen.getByText("Avatar message")).toBeInTheDocument();
        expect(screen.queryByTestId("action-type")).toBeNull();
    });

    it("renders a message for mobile view", () => {
        render(
            <Toast
                type={ToastType.SUCCESS}
                message="Mobile view message"
                mobile={true}
            />,
        );
        // For a mobile view test, you might check for specific styling changes or layout differences.
        expect(screen.getByText("Mobile view message")).toBeInTheDocument();
        // Add additional checks for mobile-specific assertions if needed
    });

    it("renders a mobile view success message", () => {
        render(
            <Toast
                type={ToastType.SUCCESS}
                message="Mobile success message"
                mobile={true}
            />,
        );
        const mobileNotification = screen.getByText("Mobile success message").parentElement
            ?.parentElement;
        expect(mobileNotification).toBeInTheDocument();
        expect(mobileNotification).toHaveStyle({ width: "341px" });
    });

    it("renders a mobile view success message with an action", () => {
        const description = "Mobile success message with action";
        render(
            <Toast
                type={ToastType.SUCCESS}
                message="Mobile action"
                description={description}
                action={true}
                mobile={true}
            />,
        );
        const mobileNotification =
            screen.getByText(description)?.parentElement?.parentElement;
        expect(mobileNotification).toBeInTheDocument();
        expect(mobileNotification).toHaveStyle({ width: "341px" });
        expect(screen.getByTestId("action-type")).toBeInTheDocument();
    });

    it("renders a mobile view warning message", () => {
        render(
            <Toast
                type={ToastType.WARNING}
                message="Mobile warning message"
                mobile={true}
            />,
        );
        const mobileNotification = screen.getByText("Mobile warning message").parentElement
            ?.parentElement;
        expect(mobileNotification).toBeInTheDocument();
        expect(mobileNotification).toHaveStyle({ width: "341px" });
    });

    it("renders a mobile view warning message with an action and description", () => {
        const description = "Mobile warning message with action";
        render(
            <Toast
                type={ToastType.WARNING}
                message="Mobile warning action"
                description={description}
                action={true}
                mobile={true}
            />,
        );
        const mobileNotification =
            screen.getByText(description)?.parentElement?.parentElement;
        expect(mobileNotification).toBeInTheDocument();
        expect(mobileNotification).toHaveStyle({ width: "341px" });
        expect(screen.getByTestId("action-type")).toBeInTheDocument();
    });

    it("renders a mobile view avatar message with an action", () => {
        const message = "Mobile avatar message";
        const description = "Hi Neil, mobile avatar description.";
        render(
            <Toast
                type={ToastType.AVATAR}
                message={message}
                description={description}
                action={true}
                mobile={true}
            />,
        );
        const mobileNotification = screen.getByText(message)?.parentElement?.parentElement;
        expect(mobileNotification).toBeInTheDocument();
        expect(mobileNotification).toHaveStyle({ width: "341px" });
        expect(screen.getByTestId("action-type")).toBeInTheDocument();
    });
});
